def func_6(input):
    if type(input) == str:
        return lambda a, b: (-1 if len(a) < len(b) else (0 if len(a) == len(b) else 1))
    else:
        return lambda a, b: (-1 if a < b else (0 if a == b else 1))


lit = func_6('lit')
print(lit('Ion are multe', 'Vasile nu arel'))

cif = func_6(11)
print(cif(31, 21))
