

def func_1(*args):
    mini = args[0]
    maxi = args[0]
    for el in args:
        if mini > el:
            mini = el
        if maxi < el:
            maxi = el

    tup = (mini, maxi)
    return tup


print('Mine:', func_1(55, -6, 33, 12, 7.5))


def func_2(*args):
    tup = (min(args), max(args))
    return tup


print('Build-in:', func_2(55, -6, 33, 12, 7.5))
