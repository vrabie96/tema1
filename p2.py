import sys


def func_1(str1, str2):
    Lines = []
    with open(str1, mode='r') as file:
        for line in file:
            if str2.rstrip() in line:
                Lines.append(line)
    return Lines


print(func_1(sys.argv[1], ' '.join(sys.argv[2:])))
