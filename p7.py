dicti = {'alex': ['075523231', '07563623'], 'Ion': ['0742352243']}

print(dicti)


def show(di):
    for el, nr in di.items():
        print(el, ':', end='')
        for i in nr:
            print(i, ' ', end='')
        print()


def addCont(di):
    el = input('name: ')
    nr = input('nr:')
    if el in di:
        di[el].append(nr)
    else:
        di[el] = [nr]


def addNum(di):
    el = input('name: ')
    nr = input('nr:')
    if el in di:
        di[el].append(nr)
    else:
        print('\n---You dont have this contatc--\n')


def findCont(di):
    el = input('Name: ')
    if el in di:
        print(el, ':', di[el])
    else:
        print('\n---You dont have this contatc--\n')


def deleteCont(di):
    el = input('Name: ')
    di.pop(el)


while True:
    print('*' * 10, 'MENU', '*' * 10)
    print('1 - Show agenda')
    print('2 - Add contact')
    print('3 - Add phone nr. to contact')
    print('4 - Find contact')
    print('5 - Delete contatc')
    print('0 - Exit')
    print('*' * 25)
    n = input('Your option: ')
    if n == '0':
        break
    elif n == '1':
        show(dicti)
    elif n == '2':
        addCont(dicti)
    elif n == '3':
        addNum(dicti)
    elif n == '4':
        findCont(dicti)
    elif n == '5':
        deleteCont(dicti)
