def func_4(file1, file2, file3):
    with open(file1, mode='r') as headler1:
        with open(file2, mode='r') as headler2:
            with open(file3, mode='w') as headler3:

                for line2 in headler2:
                    ok = 0
                    for line1 in headler1:
                        if (line2.rstrip() in line1.rstrip()):
                            ok = 1
                            break
                    if ok == 1:
                        headler3.write(line2)


func_4('countries.txt', 'visited.txt', 'to_be_visited.txt')
